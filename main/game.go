package main

import (
	"runtime"
)

var worldP *world
var tesselatorP *tesselator

func main() {
	worldP = newWorld()
	tesselatorP = newTesselator(worldP)
	gameLoop()
}

func init() {
	// GLFW event handling must run on the main OS thread
	runtime.LockOSThread()

}

func gameLoop() {
	for {
		worldP.tick()
		worldP.render(tesselatorP)
	}
}
