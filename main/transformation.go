package main

import (
	"github.com/go-gl/gl/v2.1/gl"
	"github.com/go-gl/mathgl/mgl32"
)

type transformation struct {
	position   mgl32.Vec3
	rotation   mgl32.Vec3
	quaternion mgl32.Quat
	scale      mgl32.Vec3

	Up      mgl32.Vec3
	Right   mgl32.Vec3
	Forward mgl32.Vec3

	transformMatrix  mgl32.Mat4
	transformUniform int32

	tmpVector mgl32.Vec3
}

func newTransformation(program uint32) *transformation {
	transformation := &transformation{
		position:   mgl32.Vec3{0, 0, 0},
		rotation:   mgl32.Vec3{0, 0, 0},
		quaternion: mgl32.QuatIdent(),
		scale:      mgl32.Vec3{1, 1, 1},

		Up:      mgl32.Vec3{0, 1, 0},
		Right:   mgl32.Vec3{1, 0, 0},
		Forward: mgl32.Vec3{0, 0, -1},

		transformMatrix: mgl32.Ident4(),

		tmpVector: mgl32.Vec3{0, 0, 0},
	}
	transformation.transformUniform = gl.GetUniformLocation(program, gl.Str("transformation\x00"))
	gl.UniformMatrix4fv(transformation.transformUniform, 1, false, &transformation.transformMatrix[0])

	return transformation
}

func (transformation *transformation) translate(x, y, z float32) {
	transformation.tmpVector[0] = x
	transformation.tmpVector[1] = y
	transformation.tmpVector[2] = z
	transformation.position = transformation.position.Add(transformation.tmpVector)
}

func (transformation *transformation) rotateY(angle float32) {
	transformation.rotation[1] += angle

	v1 := mgl32.Vec3{0, 1, 0}
	transformation.rotateOnAxis(v1, angle)
}

func (transformation *transformation) update() {
	transformation.transformMatrix[12] = transformation.position[0]
	transformation.transformMatrix[13] = transformation.position[1]
	transformation.transformMatrix[14] = transformation.position[2]

	gl.UniformMatrix4fv(transformation.transformUniform, 1, false, &transformation.transformMatrix[0])
}

func (transformation *transformation) rotateOnAxis(axis mgl32.Vec3, angle float32) {
	q1 := mgl32.QuatRotate(angle, axis)
	transformation.quaternion = transformation.quaternion.Mul(q1)

	transformation.transformMatrix = transformation.transformMatrix.Mul4(q1.Mat4())
}
