#version 400


uniform mat4 projection;
uniform mat4 transformation;
uniform mat4 model;
in vec3 vert;
in vec2 vertTexCoord;
in vec3 vertNormal;
in vec4 vertColor;
out vec2 fragTexCoord;
out vec4 fragPosition;
out vec3 fragNormal;
out vec4 fragColor;


void main() {
    fragTexCoord = vertTexCoord;
    gl_Position = projection * transformation * model * vec4(vert, 1);
		fragPosition = gl_Position;
		fragNormal = vertNormal;
		fragColor = vertColor;
}
