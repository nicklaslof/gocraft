package main

// Width of a chunk
const chunkwidth = 16

// Height of a chunk
const chunkheight = 32

type chunk struct {
	chunkmap     [(chunkwidth * chunkwidth) * chunkheight]byte
	rebuild      bool
	vao          uint32
	vaoindex     uint32
	vaosize      int32
	vaoindexsize int32
	xpos         int
	zpos         int
	worldXPos    int
	worldZPos    int
}

func (c *chunk) getBlock(x int, y int, z int) byte {
	if y == -1 {
		return 0
	}
	if x < 0 || x >= chunkwidth || z < 0 || z >= chunkwidth || y < 0 || y >= chunkheight {
		return 0
	}

	return c.chunkmap[(x*chunkwidth+z)+(y*chunkwidth*chunkwidth)]

}

func (c *chunk) setBlock(x int, y int, z int, block byte) {
	if x < 0 || x >= chunkwidth || z < 0 || z >= chunkwidth || y < 0 || y >= chunkheight {
		println("out of bounds")
	} else {
		c.chunkmap[(x*chunkwidth+z)+(y*chunkwidth*chunkwidth)] = block
		c.rebuild = true
	}
}
