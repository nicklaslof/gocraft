package main

import (
	"math"

	"github.com/go-gl/gl/v2.1/gl"
	"github.com/go-gl/mathgl/mgl32"
)

type camera struct {
	transformation   *transformation
	projectionMatrix mgl32.Mat4
	tmpVector        mgl32.Vec3

	projectionUniform int32

	frustum *frustum

	pendingUpdate bool
}

func newCamera(program uint32) *camera {

	camera := &camera{
		tmpVector:      mgl32.Vec3{0, 0, 0},
		pendingUpdate:  true,
		transformation: newTransformation(program),
	}

	var near float32 = 0.1
	var far float32 = 1000.0
	var fov float32 = 40.0
	var aspect float32 = 1280 / 480

	ymax := near * float32(math.Tan(float64(mgl32.DegToRad(fov*0.5))))
	ymin := -ymax
	xmin := ymin * aspect
	xmax := ymax * aspect

	camera.projectionMatrix = mgl32.Frustum(xmin, xmax, ymin, ymax, near, far)

	camera.projectionUniform = gl.GetUniformLocation(program, gl.Str("projection\x00"))
	gl.UniformMatrix4fv(camera.projectionUniform, 1, false, &camera.projectionMatrix[0])
	camera.frustum = new(frustum)
	return camera
}

func (camera *camera) translate(x, y, z float32) {
	camera.transformation.translate(x, y, z)
	camera.pendingUpdate = true
}

func (camera *camera) rotateY(angle float32) {
	camera.transformation.rotateY(angle)
	camera.pendingUpdate = true
}

func (camera *camera) update() {
	if camera.pendingUpdate {

		m := mgl32.Mat4(camera.projectionMatrix)
		m = m.Mul4(camera.transformation.transformMatrix)
		m.Inv()

		camera.frustum.updateFromViewMatrix(&m)
		camera.pendingUpdate = false
	}

	camera.transformation.update()

}

func (camera *camera) chunkInFrustum(chunk *chunk, radius float32) bool {
	v := mgl32.Vec3{float32(chunk.worldXPos + 8), 8, float32(chunk.worldZPos + 8)}
	return camera.frustum.sphereInFrustum(v, radius)
}
