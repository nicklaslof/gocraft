package main

import (
	"fmt"

	"github.com/go-gl/glfw/v3.1/glfw"
)

type world struct {
	openGlWindow *glfw.Window
	chunks       []*chunk
	camera       *camera
	window       *glfw.Window
}

var (
	velocity    float32 = 3.0
	currentTime int64
)

func newWorld() *world {

	w := &world{}

	for index := -12; index < 12; index++ {
		for index2 := -12; index2 < 12; index2++ {
			var c = w.createChunk(index, index2)
			w.chunks = append(w.chunks, c)
		}

	}

	//w.chunks = append(w.chunks, w.createChunk(1, 1))

	fmt.Println(len(w.chunks), " chunks created")

	return w

}

func (w *world) createChunk(xpos int, zpos int) *chunk {
	c := &chunk{}

	c.xpos = xpos
	c.zpos = zpos
	c.worldXPos = xpos * 16
	c.worldZPos = zpos * 16

	for x := 0; x < chunkwidth; x++ {
		for z := 0; z < chunkwidth; z++ {
			for y := 0; y < chunkheight; y++ {
				if y == 0 {
					c.setBlock(x, y, z, 1)
				} else {
					var freq = 0.003
					var weight = 1.0 - float64(float64(y)/float64(chunkheight))
					var baseDensity = 0.0
					for i := 0; i < 3; i++ {
						baseDensity = baseDensity +
							Noise3(
								float64(c.worldXPos+x)*freq,
								float64(y)*freq,
								float64(c.worldZPos+z)*freq)

						baseDensity = baseDensity * weight
						freq = freq * 1.5
						weight = weight * 0.4
					}

					baseDensity = baseDensity * 128

					if baseDensity > float64(y) {
						c.setBlock(x, y, z, 1)
					} else {
						c.setBlock(x, y, z, 0)
					}
				}

				//c.setBlock(x, y, z, 1)
			}
		}
	}

	return c
}

func (w *world) tick() {
	if w.window.GetKey(glfw.KeySpace) == glfw.Press {
		w.camera.translate(0, 0.5*velocity, 0)
	} else if w.window.GetKey(glfw.KeyLeftShift) == glfw.Press {
		w.camera.translate(0, -0.5*velocity, 0)
	}

	if w.window.GetKey(glfw.KeyW) == glfw.Press {
		w.camera.translate(0, 0, 0.5*velocity)
	} else if w.window.GetKey(glfw.KeyS) == glfw.Press {
		w.camera.translate(0, 0, -0.5*velocity)
	}

	if w.window.GetKey(glfw.KeyA) == glfw.Press {
		w.camera.translate(0.5*velocity, 0.0, 0)
	} else if w.window.GetKey(glfw.KeyD) == glfw.Press {
		w.camera.translate(-0.5*velocity, 0, 0)
	}

	if w.window.GetKey(glfw.KeyQ) == glfw.Press {
		w.camera.rotateY(-0.01)
	} else if w.window.GetKey(glfw.KeyE) == glfw.Press {
		w.camera.rotateY(0.01)
	}

}

func (w *world) render(t *tesselator) {
	t.render(w)
}
