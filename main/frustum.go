package main

import "github.com/go-gl/mathgl/mgl32"

type plane struct {
	normal   mgl32.Vec3
	distance float32
}

type frustum struct {
	planes     []plane
	initalized bool
}

func updatePlane(plane plane, normalx, normaly, normalz float32, distance float32) plane {

	plane.distance = distance
	plane.normal = mgl32.Vec3{normalx, normaly, normalz}

	return plane
}

func (frustum *frustum) updateFromViewMatrix(mat *mgl32.Mat4) {
	if !frustum.initalized {
		frustum.planes = make([]plane, 6, 6)
		for i := range frustum.planes {
			plane := new(plane)
			frustum.planes[i] = *plane
		}
		frustum.initalized = true
	}

	frustum.planes[0] = updatePlane(frustum.planes[0], mat[3]-mat[0], mat[7]-mat[4], mat[11]-mat[8], mat[15]-mat[12])
	frustum.planes[1] = updatePlane(frustum.planes[1], mat[3]+mat[0], mat[7]+mat[4], mat[11]+mat[8], mat[15]+mat[12])

	frustum.planes[2] = updatePlane(frustum.planes[2], mat[3]+mat[1], mat[7]+mat[5], mat[11]+mat[9], mat[15]+mat[13])
	frustum.planes[3] = updatePlane(frustum.planes[3], mat[3]-mat[1], mat[7]-mat[5], mat[11]-mat[9], mat[15]-mat[13])

	frustum.planes[4] = updatePlane(frustum.planes[4], mat[3]-mat[2], mat[7]-mat[6], mat[11]-mat[10], mat[15]-mat[14])
	frustum.planes[5] = updatePlane(frustum.planes[5], mat[3]+mat[2], mat[7]+mat[6], mat[11]+mat[10], mat[15]+mat[14])
}

func (plane *plane) signedDistanceToPoint(pt mgl32.Vec3) float32 {
	return (plane.normal.Dot(pt) + plane.distance)
}

func (frustum *frustum) sphereInFrustum(center mgl32.Vec3, radius float32) bool {
	for _, plane := range frustum.planes {
		var dot = center.Dot(plane.normal)
		if dot+plane.distance < -64 {
			return false
		}
	}
	return true
}
