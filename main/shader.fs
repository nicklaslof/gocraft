#version 400

uniform sampler2D tex;
in vec2 fragTexCoord;
in vec4 fragPosition;
in vec3 fragNormal;
in vec4 fragColor;
out vec4 outputColor;
void main() {
    //outputColor = texture(tex, fragTexCoord);
    //outputColor = vec4(1.0,fragPosition.y/4.0,0.0,1.0);
		//outputColor = fragColor;
	  vec4 color = vec4(fragColor);

		// Fake shadows based on normals
		if (fragNormal.x < 0.0 || fragNormal.x >0.0){
    	color -= 0.05;
    }

		if (fragNormal.z < 0.0 || fragNormal.z >0.0){
    	color -= 0.03;
    }

		outputColor = color;
}
