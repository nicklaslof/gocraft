package main

import (
	"fmt"
	"log"

	"github.com/go-gl/gl/v2.1/gl"
	"github.com/go-gl/glfw/v3.1/glfw"
	"github.com/go-gl/mathgl/mgl32"
)

type tesselator struct {
	openGlWindow       *glfw.Window
	previousTime       float64
	angle              float64
	program            uint32
	vao                uint32
	vaoindex           uint32
	vertices           []float32
	indices            []uint16
	vertexindex        int
	indexcounter       uint16
	newIndex           bool
	points             []mgl32.Vec3
	modelMatrixUniform int32
	modelMatrix        mgl32.Mat4
}

var frontNormal = []float32{0.0, 0.0, 1.0}
var backNormal = []float32{0.0, 0.0, -1.0}
var rightNormal = []float32{1.0, 0.0, 0.0}
var leftNormal = []float32{-1.0, 0.0, 1.0}
var topNormal = []float32{0.0, 1.0, 0.0}
var bottomNormal = []float32{0.0, -1.0, 0.0}

var green = []float32{0.0, 0.3, 0.0, 1.0}
var red = []float32{1.0, 0.0, 0.0, 1.0}
var blue = []float32{0.0, 0.0, 1.0, 1.0}

var size float32 = 1.0

var visibleChunks = 0
var invisibleChunks = 0
var elapsedSinceLastSecond = 0.0
var secondTick = 0

func newTesselator(world *world) *tesselator {

	t := &tesselator{}

	if err := glfw.Init(); err != nil {
		log.Fatalln("failed to initialize glfw:", err)
	}

	glfw.WindowHint(glfw.Resizable, glfw.True)
	glfw.WindowHint(glfw.ContextVersionMajor, 4)
	glfw.WindowHint(glfw.ContextVersionMinor, 1)
	glfw.WindowHint(glfw.OpenGLProfile, glfw.OpenGLCoreProfile)
	glfw.WindowHint(glfw.OpenGLForwardCompatible, glfw.True)
	window, err := glfw.CreateWindow(1280, 768, "GoCraft", nil, nil)
	if err != nil {
		panic(err)
	}

	window.MakeContextCurrent()

	if err := gl.Init(); err != nil {
		panic(err)
	}

	version := gl.GoStr(gl.GetString(gl.VERSION))
	fmt.Println("OpenGL version", version)

	program, err := newProgram("shader.vs", "shader.fs")
	if err != nil {
		panic(err)
	}

	gl.UseProgram(program)

	world.camera = newCamera(program)
	world.window = window
	t.openGlWindow = window
	t.previousTime = glfw.GetTime()
	t.program = program

	gl.Enable(gl.DEPTH_TEST)
	gl.DepthFunc(gl.LESS)
	gl.Enable(gl.CULL_FACE)
	gl.CullFace(gl.BACK)

	t.modelMatrix = mgl32.Ident4()

	t.modelMatrixUniform = gl.GetUniformLocation(program, gl.Str("model\x00"))
	gl.UniformMatrix4fv(t.modelMatrixUniform, 1, false, &t.modelMatrix[0])
	return t
}

func (t *tesselator) render(world *world) {
	time := glfw.GetTime()
	var elapsed = time - t.previousTime
	t.previousTime = time
	elapsedSinceLastSecond = elapsedSinceLastSecond + elapsed

	visibleChunks = 0
	invisibleChunks = 0

	gl.Clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT)
	gl.ClearColor(0.6, 0.6, 0.75, 1.0)

	gl.UseProgram(t.program)

	world.camera.update()

	for _, chunk := range world.chunks {
		if chunk.rebuild {
			t.rebuildChunk(chunk)
			chunk.vao, chunk.vaoindex = t.buildVao()
			chunk.vaosize = int32(len(t.vertices))
			chunk.vaoindexsize = int32(len(t.indices))
			chunk.rebuild = false
		} else {

			if world.camera.chunkInFrustum(chunk, 16) {
				visibleChunks = visibleChunks + 1
				var vao = chunk.vao

				gl.BindVertexArray(vao)
				gl.EnableVertexAttribArray(0)

				var vaoindex = chunk.vaoindex
				gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, vaoindex)
				gl.DrawElements(gl.TRIANGLES, chunk.vaoindexsize, gl.UNSIGNED_SHORT, gl.PtrOffset(0))

				gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, 0)
				gl.DisableVertexAttribArray(0)
				gl.BindVertexArray(0)
			} else {
				invisibleChunks = invisibleChunks + 1
			}
		}
	}

	t.openGlWindow.SwapBuffers()
	glfw.PollEvents()

	if elapsedSinceLastSecond >= 1.0 {
		secondTick = secondTick + 1
		elapsedSinceLastSecond = elapsedSinceLastSecond - 1.0

		fmt.Println("Visible chunks: ", visibleChunks, " Invisible chunks: ", invisibleChunks, " FPS: ", (1000/elapsed)/1000)
	}
}

func (t *tesselator) rebuildChunk(c *chunk) {

	t.vertexindex = 0
	t.vertices = make([]float32, 0, 0)
	t.indices = make([]uint16, 0, 0)
	t.indexcounter = 0
	t.points = make([]mgl32.Vec3, 8, 8)

	for x := 0; x < chunkwidth; x++ {
		for z := 0; z < chunkwidth; z++ {
			for y := 0; y < chunkheight; y++ {
				var block = c.getBlock(x, y, z)
				if block != 0 {
					t.generateCube(c, x, y, z)
				}
			}
		}
	}
}

func (t *tesselator) generateCube(c *chunk, x int, y int, z int) {
	var chunkXPos = c.worldXPos
	var chunkZPos = c.worldZPos

	t.points[0] = mgl32.Vec3{float32(x + chunkXPos), float32(y), float32(z+chunkZPos) + size}
	t.points[1] = mgl32.Vec3{float32(x+chunkXPos) + size, float32(y), float32(z+chunkZPos) + size}
	t.points[2] = mgl32.Vec3{float32(x+chunkXPos) + size, float32(y) + size, float32(z+chunkZPos) + size}
	t.points[3] = mgl32.Vec3{float32(x + chunkXPos), float32(y) + size, float32(z+chunkZPos) + size}
	t.points[4] = mgl32.Vec3{float32(x+chunkXPos) + size, float32(y), float32(z + chunkZPos)}
	t.points[5] = mgl32.Vec3{float32(x + chunkXPos), float32(y), float32(z + chunkZPos)}
	t.points[6] = mgl32.Vec3{float32(x + chunkXPos), float32(y) + size, float32(z + chunkZPos)}
	t.points[7] = mgl32.Vec3{float32(x+chunkXPos) + size, float32(y) + size, float32(z + chunkZPos)}

	if c.getBlock(x, y+1, z) == 0 {
		t.addTop(t.points)
	}
	if c.getBlock(x, y-1, z) == 0 {
		t.addBottom(t.points)
	}
	if c.getBlock(x, y, z+1) == 0 {
		t.addFront(t.points)
	}
	if c.getBlock(x, y, z-1) == 0 {
		t.addBack(t.points)
	}
	if c.getBlock(x+1, y, z) == 0 {
		t.addRight(t.points)
	}
	if c.getBlock(x-1, y, z) == 0 {
		t.addLeft(t.points)
	}
}

func (t *tesselator) buildVao() (uint32, uint32) {

	gl.GenVertexArrays(1, &t.vao)
	gl.BindVertexArray(t.vao)

	var vbo uint32
	gl.GenBuffers(1, &vbo)
	gl.BindBuffer(gl.ARRAY_BUFFER, vbo)

	gl.BufferData(gl.ARRAY_BUFFER, len(t.vertices)*4, gl.Ptr(t.vertices), gl.STATIC_DRAW)

	vertAttrib := uint32(gl.GetAttribLocation(t.program, gl.Str("vert\x00")))
	gl.EnableVertexAttribArray(vertAttrib)
	gl.VertexAttribPointer(vertAttrib, 3, gl.FLOAT, false, 12*4, gl.PtrOffset(0))

	texCoordAttrib := uint32(gl.GetAttribLocation(t.program, gl.Str("vertTexCoord\x00")))
	gl.EnableVertexAttribArray(texCoordAttrib)
	gl.VertexAttribPointer(texCoordAttrib, 2, gl.FLOAT, false, 12*4, gl.PtrOffset(3*4))

	normalAttrib := uint32(gl.GetAttribLocation(t.program, gl.Str("vertNormal\x00")))
	gl.EnableVertexAttribArray(normalAttrib)
	gl.VertexAttribPointer(normalAttrib, 3, gl.FLOAT, false, 12*4, gl.PtrOffset(5*4))

	colorAttrib := uint32(gl.GetAttribLocation(t.program, gl.Str("vertColor\x00")))
	gl.EnableVertexAttribArray(colorAttrib)
	gl.VertexAttribPointer(colorAttrib, 4, gl.FLOAT, false, 12*4, gl.PtrOffset(8*4))

	var vboindex uint32
	gl.GenBuffers(1, &vboindex)
	gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, vboindex)
	gl.BufferData(gl.ELEMENT_ARRAY_BUFFER, len(t.indices)*2, gl.Ptr(t.indices), gl.STATIC_DRAW)

	t.vaoindex = vboindex

	return t.vao, t.vaoindex
}

func (t *tesselator) setVertex(x float32, y float32, z float32, u float32, v float32, normals []float32, color []float32) {
	t.vertices = append(t.vertices, x, y, z, u, v)
	t.vertices = append(t.vertices, normals...)
	t.vertices = append(t.vertices, color...)
	t.indexcounter = t.indexcounter + 1
}

func (t *tesselator) setIndex(i ...uint16) {
	t.indices = append(t.indices, i...)
}

func (t *tesselator) addTop(points []mgl32.Vec3) {
	var indexCounter = t.indexcounter
	t.setVertex(points[3].X(), points[3].Y(), points[3].Z(), 0.0, 0.0, topNormal, green)
	t.setVertex(points[2].X(), points[2].Y(), points[2].Z(), 0.0, 0.0, topNormal, green)
	t.setVertex(points[7].X(), points[7].Y(), points[7].Z(), 0.0, 0.0, topNormal, green)
	t.setVertex(points[6].X(), points[6].Y(), points[6].Z(), 0.0, 0.0, topNormal, green)
	t.setIndex(indexCounter, indexCounter+1, indexCounter+2, indexCounter+2, indexCounter+3, indexCounter)
}

func (t *tesselator) addBottom(points []mgl32.Vec3) {
	var indexCounter = t.indexcounter
	t.setVertex(points[5].X(), points[5].Y(), points[5].Z(), 0.0, 0.0, bottomNormal, green)
	t.setVertex(points[4].X(), points[4].Y(), points[4].Z(), 0.0, 0.0, bottomNormal, green)
	t.setVertex(points[1].X(), points[1].Y(), points[1].Z(), 0.0, 0.0, bottomNormal, green)
	t.setVertex(points[0].X(), points[0].Y(), points[0].Z(), 0.0, 0.0, bottomNormal, green)
	t.setIndex(indexCounter, indexCounter+1, indexCounter+2, indexCounter+2, indexCounter+3, indexCounter)
}

func (t *tesselator) addFront(points []mgl32.Vec3) {
	var indexCounter = t.indexcounter
	t.setVertex(points[0].X(), points[0].Y(), points[0].Z(), 0.0, 0.0, frontNormal, green)
	t.setVertex(points[1].X(), points[1].Y(), points[1].Z(), 0.0, 0.0, frontNormal, green)
	t.setVertex(points[2].X(), points[2].Y(), points[2].Z(), 0.0, 0.0, frontNormal, green)
	t.setVertex(points[3].X(), points[3].Y(), points[3].Z(), 0.0, 0.0, frontNormal, green)
	t.setIndex(indexCounter, indexCounter+1, indexCounter+2, indexCounter+2, indexCounter+3, indexCounter)
}

func (t *tesselator) addBack(points []mgl32.Vec3) {
	var indexCounter = t.indexcounter
	t.setVertex(points[4].X(), points[4].Y(), points[4].Z(), 0.0, 0.0, backNormal, green)
	t.setVertex(points[5].X(), points[5].Y(), points[5].Z(), 0.0, 0.0, backNormal, green)
	t.setVertex(points[6].X(), points[6].Y(), points[6].Z(), 0.0, 0.0, backNormal, green)
	t.setVertex(points[7].X(), points[7].Y(), points[7].Z(), 0.0, 0.0, backNormal, green)
	t.setIndex(indexCounter, indexCounter+1, indexCounter+2, indexCounter+2, indexCounter+3, indexCounter)
}
func (t *tesselator) addLeft(points []mgl32.Vec3) {
	var indexCounter = t.indexcounter
	t.setVertex(points[5].X(), points[5].Y(), points[5].Z(), 0.0, 0.0, leftNormal, green)
	t.setVertex(points[0].X(), points[0].Y(), points[0].Z(), 0.0, 0.0, leftNormal, green)
	t.setVertex(points[3].X(), points[3].Y(), points[3].Z(), 0.0, 0.0, leftNormal, green)
	t.setVertex(points[6].X(), points[6].Y(), points[6].Z(), 0.0, 0.0, leftNormal, green)
	t.setIndex(indexCounter, indexCounter+1, indexCounter+2, indexCounter+2, indexCounter+3, indexCounter)
}
func (t *tesselator) addRight(points []mgl32.Vec3) {
	var indexCounter = t.indexcounter
	t.setVertex(points[1].X(), points[1].Y(), points[1].Z(), 0.0, 0.0, rightNormal, green)
	t.setVertex(points[4].X(), points[4].Y(), points[4].Z(), 0.0, 0.0, rightNormal, green)
	t.setVertex(points[7].X(), points[7].Y(), points[7].Z(), 0.0, 0.0, rightNormal, green)
	t.setVertex(points[2].X(), points[2].Y(), points[2].Z(), 0.0, 0.0, rightNormal, green)
	t.setIndex(indexCounter, indexCounter+1, indexCounter+2, indexCounter+2, indexCounter+3, indexCounter)
}
